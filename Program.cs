﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace FifthLessonOTUS_7
{
    class Program
    {
        static void Main(string[] args)
        {
            HttpClient client = new HttpClient();
            while (true)
            {
                Console.Write("Type-in any URL-address for searching: ");
                string userInput = Console.ReadLine();
                Uri url = new Uri(userInput.Contains("http") ? userInput : "http://" + userInput);

                var httpContent = client.GetAsync(url).GetAwaiter().GetResult().Content.ReadAsStringAsync().GetAwaiter().GetResult();
                var regExp = @"(""[\s]+)?(?<1>([a-zA-Z]+:\/\/)([a-zA-Z0-9]([a-zA-Z0-9-_.:]+)?[a-zA-Z0-9!*'():;@&+=$,/?#\[\]._~%-]+))";
                var foundUrls = Regex.Match(httpContent, regExp, RegexOptions.IgnoreCase | RegexOptions.Compiled);
                int i = 1;
                Console.WriteLine("Found URLs");
                while (foundUrls.Success)
                {
                    Console.WriteLine(i + ") " + foundUrls.Groups[1]);
                    foundUrls = foundUrls.NextMatch();
                    i++;
                }
                Console.WriteLine();
            }
        }
    }
}
